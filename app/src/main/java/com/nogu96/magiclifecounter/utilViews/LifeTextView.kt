package com.nogu96.magiclifecounter.utilViews

import android.content.Context
import android.util.AttributeSet

class LifeTextView: androidx.appcompat.widget.AppCompatTextView {

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?): super(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int): super(context, attrs, defStyleAttr) {

    }

    fun set(life: Int) {
        text = life.toString()

    }

}