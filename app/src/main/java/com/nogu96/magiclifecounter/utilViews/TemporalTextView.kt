package com.nogu96.magiclifecounter.utilViews

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.nogu96.magiclifecounter.R

class TemporalTextView: androidx.appcompat.widget.AppCompatTextView {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?): super(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int): super(context, attrs, defStyleAttr)

    fun set(value: Int) {
        if (value == 0) {
            visibility = View.GONE
            return
        }

        var string = value.toString()
        if (value > 0) {
            setTextColor(ContextCompat.getColor(context, R.color.green))
            string = '+' + string
        } else {
            setTextColor(ContextCompat.getColor(context, R.color.red))
        }
        text = string

        if (visibility == View.GONE)
            visibility = View.VISIBLE
    }
}