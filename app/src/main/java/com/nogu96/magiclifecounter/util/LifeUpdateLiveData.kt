package com.nogu96.magiclifecounter.util

import android.os.CountDownTimer
import androidx.lifecycle.MutableLiveData

class LifeUpdateLiveData(
    val listener: Listener
): MutableLiveData<Int>() {

    private val TIME_LIMIT: Long = 1500

    var timer: CountDownTimer? = null

    init {
        setValue(0)
    }

    override fun setValue(value: Int?) {
        super.setValue(value)
        if (value == null) {
            return
        }
        timer?.cancel()
        if (value == 0) {
            return
        }
        timer = object: CountDownTimer(TIME_LIMIT, TIME_LIMIT) {
            override fun onTick(p0: Long) {
                //nothing to do
            }
            override fun onFinish() {
                listener.onUpdate(value)
                this@LifeUpdateLiveData.setValue(0)
            }
        }.start()
    }

    fun update(value: Int) {
        getValue()?.let {
            setValue(it + value)
        }
    }

    interface Listener {
        fun onUpdate(lifeDifference: Int)
    }

}