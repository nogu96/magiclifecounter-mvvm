package com.nogu96.magiclifecounter.util

import androidx.lifecycle.MutableLiveData

fun MutableLiveData<*>.notifyObservers() {
    value = value
}