package com.nogu96.magiclifecounter.KoinModule

import com.nogu96.magiclifecounter.MainViewModel
import com.nogu96.magiclifecounter.fragment.LifeRegisterViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val MainViewModelModule = module {
    viewModel {
        MainViewModel()
    }

    single {
        LifeRegisterViewModel()
    }
}