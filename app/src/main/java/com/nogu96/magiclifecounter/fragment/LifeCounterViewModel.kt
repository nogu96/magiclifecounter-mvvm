package com.nogu96.magiclifecounter.fragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nogu96.magiclifecounter.model.DAMAGE_TYPE
import com.nogu96.magiclifecounter.model.NUMBER
import com.nogu96.magiclifecounter.model.Player
import com.nogu96.magiclifecounter.util.LifeUpdateLiveData

class LifeCounterViewModel constructor(number: NUMBER): ViewModel(), LifeUpdateLiveData.Listener {

    val INITIAL_LIFE = 20

    private val player = Player(number, INITIAL_LIFE, 0)
    var playerLife: LiveData<Int> = player.life
    val damageType: LiveData<DAMAGE_TYPE> = player.damageType
    val lifeChange: LifeUpdateLiveData = LifeUpdateLiveData(this)
    private val _lifeDifference: MutableLiveData<Int> = MutableLiveData()
    val lifeDiference: LiveData<Int> = _lifeDifference

    fun gain1() {
        gain(1)
    }

    fun gain5() {
        gain(5)
    }

    fun loss1() {
        loss(1)
    }

    fun loss5() {
        loss(5)
    }

    fun reset() {
        player.reset()
    }

    fun setLifeState() {
        player.updateDamageState(DAMAGE_TYPE.LIFE)
    }

    fun setInfectState() {
        player.updateDamageState(DAMAGE_TYPE.INFECT)
    }

    /********
     * LifeDataUpdate.Listener
     *******/
    override fun onUpdate(lifeDifference: Int) {
        _lifeDifference.value = lifeDifference
    }

    /******************
     * private methods
     ******************/
    private fun gain(value: Int) {
        player.gain(value)
        lifeChange.update(value)
    }

    private fun loss(value: Int) {
        player.loss(value)
        lifeChange.update(value * (-1))
    }

}