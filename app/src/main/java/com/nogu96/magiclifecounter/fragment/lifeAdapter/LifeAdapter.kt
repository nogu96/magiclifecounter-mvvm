package com.nogu96.magiclifecounter.fragment.lifeAdapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.nogu96.magiclifecounter.R
import com.nogu96.magiclifecounter.databinding.RecyclerItemBinding
import com.nogu96.magiclifecounter.model.LifeItem
import com.nogu96.magiclifecounter.model.NUMBER

class LifeAdapter: RecyclerView.Adapter<LifeAdapter.ViewHolder>() {

    val lifeList: MutableList<LifeItem> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RecyclerItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        ))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(lifeList[position])
    }

    override fun getItemCount(): Int = lifeList.count()

    fun add(life: LifeItem) {
        lifeList.add(life)
        notifyItemChanged(lifeList.size)
    }

    fun reset() {
        lifeList.clear()
        notifyDataSetChanged()
    }

    class ViewHolder(val binding: RecyclerItemBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(life: LifeItem) {
            val view = when(life.NUMBER) {
                NUMBER.ONE -> {
                    binding.rightText.visibility = View.INVISIBLE
                    binding.leftText.visibility = View.VISIBLE
                    binding.leftText
                }
                NUMBER.TWO -> {
                    binding.leftText.visibility = View.INVISIBLE
                    binding.rightText.visibility = View.VISIBLE
                    binding.rightText
                }
            }

            view.apply {
                val isPositive = life.value >= 0
                setTextColor(ContextCompat.getColor(context, if(isPositive) R.color.green else R.color.red))
                text = if (isPositive) "+"+life.value else life.value.toString()
            }
        }
    }

}