package com.nogu96.magiclifecounter.fragment

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nogu96.magiclifecounter.model.LifeItem

class LifeRegisterViewModel: ViewModel() {

    val newUpdate: MutableLiveData<LifeItem> = MutableLiveData()
    val reset: MutableLiveData<Boolean> = MutableLiveData(false)

    fun newUpdate(life: LifeItem) {
        newUpdate.value = life
    }

    fun reset() {
        reset.value?.let {
            reset.value = !it
        }
    }
}