package com.nogu96.magiclifecounter.fragment.lifeAdapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class DataBinder <T: RecyclerView.ViewHolder> (val databinderAdapter: DataBindAdapter) {

    abstract fun newViewHolder(parent: ViewGroup, viewType: Int): T
    abstract fun bindViewHolder(holder: T, position: Int)
    abstract fun getItemCount(): Int
}