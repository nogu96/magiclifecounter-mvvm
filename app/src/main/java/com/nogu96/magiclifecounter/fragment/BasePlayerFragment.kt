package com.nogu96.magiclifecounter.fragment

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.nogu96.magiclifecounter.R
import com.nogu96.magiclifecounter.model.DAMAGE_TYPE
import com.nogu96.magiclifecounter.model.LifeItem
import com.nogu96.magiclifecounter.model.NUMBER
import com.nogu96.magiclifecounter.utilViews.LifeTextView
import com.nogu96.magiclifecounter.utilViews.TemporalTextView

abstract class BasePlayerFragment: Fragment() {

    protected val viewModel = LifeCounterViewModel(getPlayer())
    protected abstract val lifeRegisterViewModel: LifeRegisterViewModel

    abstract fun getPlayer(): NUMBER
    abstract fun getMinus1Button(): Button
    abstract fun getMinus5Button(): Button
    abstract fun getPlus1Button(): Button
    abstract fun getPlus5Button(): Button
    abstract fun getTextLife(): LifeTextView
    abstract fun getLifeChange(): TemporalTextView
    abstract fun getLifeButton(): Button
    abstract fun getInfectButton(): Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.playerLife.observe(viewLifecycleOwner) { life ->
            life?.let {
                getTextLife().set(it)
            }
        }

        viewModel.lifeChange.observe(viewLifecycleOwner) { lifeChange ->
            lifeChange?.let {
                getLifeChange().set(it)
            }
        }

        viewModel.lifeDiference.observe(viewLifecycleOwner) { update ->
            update?.let {
                lifeRegisterViewModel.newUpdate(LifeItem(getPlayer(), it))
            }
        }

        viewModel.damageType.observe(viewLifecycleOwner) {
            when(it) {
                DAMAGE_TYPE.LIFE -> {
                    setupLife()
                }
                DAMAGE_TYPE.INFECT -> {
                    setupInfect()
                }
            }
        }

        lifeRegisterViewModel.reset.observe(viewLifecycleOwner) {
            viewModel.reset()
        }

        getMinus1Button().setOnClickListener {
            viewModel.loss1()
        }

        getMinus5Button().setOnClickListener {
            viewModel.loss5()
        }

        getPlus1Button().setOnClickListener {
            viewModel.gain1()
        }

        getPlus5Button().setOnClickListener {
            viewModel.gain5()
        }

        getLifeButton().setOnClickListener {
            viewModel.setLifeState()
        }

        getInfectButton().setOnClickListener {
            viewModel.setInfectState()
        }
    }

    /*****
     * Private methods
     ****/
    private fun setupLife() {
        context?.let {
            getTextLife().setTextColor(ContextCompat.getColor(it, R.color.green))
        }
    }

    private fun setupInfect() {
        context?.let {
            getTextLife().setTextColor(ContextCompat.getColor(it, R.color.infect))
        }
    }
}