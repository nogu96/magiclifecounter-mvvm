package com.nogu96.magiclifecounter.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nogu96.magiclifecounter.databinding.FragmentLifeCounterRightBinding
import com.nogu96.magiclifecounter.model.NUMBER
import org.koin.androidx.viewmodel.ext.android.viewModel

class RightPlayerFragment : BasePlayerFragment() {

    override val lifeRegisterViewModel: LifeRegisterViewModel by viewModel()
    private lateinit var binding: FragmentLifeCounterRightBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLifeCounterRightBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun getPlayer() = NUMBER.TWO
    override fun getMinus1Button() = binding.minus1
    override fun getMinus5Button() = binding.minus5
    override fun getPlus1Button() = binding.plus1
    override fun getPlus5Button() = binding.plus5
    override fun getTextLife() = binding.playerLife
    override fun getLifeChange() = binding.lifeChange
    override fun getLifeButton() = binding.lifeButton
    override fun getInfectButton() = binding.infectButton
}