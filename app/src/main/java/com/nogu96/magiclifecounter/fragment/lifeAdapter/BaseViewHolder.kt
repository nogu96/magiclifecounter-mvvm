package com.nogu96.magiclifecounter.fragment.lifeAdapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.nogu96.magiclifecounter.model.LifeItem

abstract class BaseViewHolder<T: LifeItem>(view: View): RecyclerView.ViewHolder(view) {
    abstract fun bind(life: T)
}