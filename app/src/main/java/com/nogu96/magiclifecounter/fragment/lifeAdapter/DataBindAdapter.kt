package com.nogu96.magiclifecounter.fragment.lifeAdapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class DataBindAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return getDataBinder<DataBinder<RecyclerView.ViewHolder>>(viewType).newViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getDataBinder<DataBinder<RecyclerView.ViewHolder>>(holder.itemViewType).bindViewHolder(holder, getBinderPosition(position))
    }

    abstract fun <T: DataBinder<RecyclerView.ViewHolder>> getDataBinder(viewType: Int): T
    abstract fun getPosition(binder: DataBinder<RecyclerView.ViewHolder>, binderPosition: Int): Int
    abstract fun getBinderPosition(position: Int): Int
}