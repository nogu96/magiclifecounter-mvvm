package com.nogu96.magiclifecounter

import android.app.Application
import com.nogu96.magiclifecounter.KoinModule.MainViewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@App)
            modules(MainViewModelModule)
        }
    }

}