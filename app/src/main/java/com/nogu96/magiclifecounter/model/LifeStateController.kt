package com.nogu96.magiclifecounter.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

enum class DAMAGE_TYPE {
    LIFE,
    INFECT
}

class LifeStateController(
    val initialLife: Int,
    val initialInfect: Int
) {
    private val lifeState: BaseCounterState = BaseCounterState(initialLife, DAMAGE_TYPE.LIFE)
    private val infectState: BaseCounterState = BaseCounterState(initialInfect, DAMAGE_TYPE.INFECT)
    private lateinit var currentState: BaseCounterState

    private val damageType: MutableLiveData<DAMAGE_TYPE> = MutableLiveData()
    private val currentLife: MutableLiveData<Int> = MutableLiveData()

    init {
        initialSetup()
    }

    fun initialSetup() {
        currentState = lifeState

        damageType.value = currentState.damagetype
        currentLife.value = currentState.currentLife
    }


    fun changeState(type: DAMAGE_TYPE) {
        when(type) {
            DAMAGE_TYPE.LIFE -> currentState = lifeState
            DAMAGE_TYPE.INFECT -> currentState = infectState
        }

        damageType.value = currentState.damagetype
        currentLife.value = currentState.currentLife
    }

    fun currentLife(): LiveData<Int> = currentLife
    fun damageType(): LiveData<DAMAGE_TYPE> = damageType
    fun gain(value: Int) {
        currentState.gain(value)
        currentLife.value = currentState.currentLife
    }
    fun loss(value: Int) {
        currentState.loss(value)
        currentLife.value = currentState.currentLife
    }
    fun didLost(): Boolean {
        val byLife = lifeState.currentLife <= 0
        val byInfect = infectState.currentLife >= 10
        return byInfect || byLife
    }
    fun reset() {
        lifeState.reset(initialLife)
        infectState.reset(initialInfect)
        initialSetup()
    }
}

class BaseCounterState(
    var currentLife: Int,
    var damagetype: DAMAGE_TYPE
) {

    fun gain(value: Int) { currentLife += value }
    fun loss(value: Int) { currentLife -= value }

    fun reset(newValue: Int) {
        currentLife = currentLife.minus(currentLife).plus(newValue)
    }
}