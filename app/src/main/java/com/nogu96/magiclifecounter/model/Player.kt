package com.nogu96.magiclifecounter.model

enum class NUMBER {
    ONE,
    TWO
}

class Player constructor(val number: NUMBER,
                         initialLife: Int,
                         initialInfect: Int) {

    private val lifeController = LifeStateController(initialLife, initialInfect)
    val life = lifeController.currentLife()
    val damageType = lifeController.damageType()

    fun getCurrentLife() = lifeController.currentLife().value!!
    fun getDamageType() = lifeController.damageType().value!!

    fun gain(value: Int) {
        lifeController.gain(value)
    }

    fun loss(value: Int) {
        lifeController.loss(value)
    }

    fun updateDamageState(type: DAMAGE_TYPE) {
        lifeController.changeState(type)
    }

    fun didLost(): Boolean {
        return lifeController.didLost()
    }

    fun reset() {
        lifeController.reset()
    }
}