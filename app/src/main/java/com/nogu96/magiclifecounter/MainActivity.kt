package com.nogu96.magiclifecounter

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.nogu96.magiclifecounter.databinding.ActivityMainBinding
import com.nogu96.magiclifecounter.fragment.LifeRegisterViewModel
import com.nogu96.magiclifecounter.fragment.lifeAdapter.LifeAdapter
import com.nogu96.magiclifecounter.model.LifeItem
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val viewModel by viewModel<MainViewModel>()
    protected val lifeRegisterViewModel: LifeRegisterViewModel by inject()
    private val lifeAdapter = LifeAdapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.lifeRecycler.apply {
            adapter = lifeAdapter
            layoutManager = LinearLayoutManager(context)
        }

        binding.resetButton.setOnClickListener {
            showResetDialog()
        }

        lifeRegisterViewModel.newUpdate.observe(this, object: Observer<LifeItem> {
            override fun onChanged(life: LifeItem?) {
                life?.let {
                    lifeAdapter.add(it)
                    binding.lifeRecycler.scrollToPosition(lifeAdapter.itemCount - 1)
                }
            }
        })
    }

    private fun showResetDialog() {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.retry_dialog_title))
            .setMessage(getString(R.string.retry_dialog_message))
            .setPositiveButton(R.string.yes) { dialog, which ->
                lifeRegisterViewModel.reset()
                lifeAdapter.reset()
            }
            .setNegativeButton(R.string.no, null)
            .show()

    }
}