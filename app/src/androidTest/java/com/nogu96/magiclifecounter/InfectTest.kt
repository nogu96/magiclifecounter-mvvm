package com.nogu96.magiclifecounter

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nogu96.magiclifecounter.model.DAMAGE_TYPE
import com.nogu96.magiclifecounter.model.NUMBER
import com.nogu96.magiclifecounter.model.Player
import io.mockk.MockKAnnotations
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class InfectTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    lateinit var player: Player
    private val INITIAL_INFECT = 0

    @Before
    fun setup() {
        player = Player(NUMBER.ONE, 20, INITIAL_INFECT)
        player.updateDamageState(DAMAGE_TYPE.INFECT)
        MockKAnnotations.init(this)
    }

    @Test
    fun test01PlayerInitialLifeIs0() {
        assert(player.getDamageType() == DAMAGE_TYPE.INFECT)
        assert(player.getCurrentLife() == INITIAL_INFECT)
    }

    @Test
    fun test02PlayerGainInfect() {
        val value = 1
        player.gain(value)
        assert(player.getCurrentLife() == INITIAL_INFECT + value)
    }

    @Test
    fun test03PlayerGainMultipleInfect() {
        val value = 1
        val count = 3
        for (i in 1..count) {
            player.gain(value)
        }
        assert(player.getCurrentLife() == count)
    }

    @Test
    fun test04PlayerLossInfect() {
        val value = 1
        player.loss(value)
        assert(player.getCurrentLife() == INITIAL_INFECT - value)
    }

    @Test
    fun test05PlayerLossMultipleInfect() {
        val value = 1
        val count = 3
        for(i in 1..count)
            player.loss(value)
        assert(player.getCurrentLife() == INITIAL_INFECT - value*3)
    }

    @Test
    fun test06PlayerDidLost() {
        player.gain(10)
        assert(player.didLost())
    }

    @Test
    fun test07ResetGame() {
        player.gain(10)
        player.reset()

        assert(player.getDamageType() == DAMAGE_TYPE.LIFE)
        player.updateDamageState(DAMAGE_TYPE.INFECT)
        assert(player.getCurrentLife() == INITIAL_INFECT)
    }

}