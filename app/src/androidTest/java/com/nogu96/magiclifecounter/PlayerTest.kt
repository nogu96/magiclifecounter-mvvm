package com.nogu96.magiclifecounter

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nogu96.magiclifecounter.model.DAMAGE_TYPE
import com.nogu96.magiclifecounter.model.Player
import com.nogu96.magiclifecounter.model.NUMBER
import io.mockk.MockKAnnotations
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class PlayerTest {

    /**
     * A JUnit Test Rule that swaps the background executor used by the Architecture Components with a different one which executes each task synchronously.
    You can use this rule for your host side tests that use Architecture Components.
     */
    @get:Rule
    val rule = InstantTaskExecutorRule()

    lateinit var player: Player
    private val INITIAL_LIFE = 20

    @Before
    fun setup() {
        player = Player(NUMBER.ONE, INITIAL_LIFE, 0)

        MockKAnnotations.init(this)
    }

    @Test
    fun test01PlayerInitialLifeIs20() {
        assert(player.getDamageType() == DAMAGE_TYPE.LIFE)
        assert(player.getCurrentLife() == INITIAL_LIFE)
    }

    @Test
    fun test02PlayerGain1Life() {
        player.gain(1)
        assert(player.getCurrentLife() == INITIAL_LIFE + 1)
    }

    @Test
    fun test03PlayerGainLifeMultipleTimes() {
        val LIMIT = 3
        for (i in 1..LIMIT) {
            player.gain(1)
        }
        assert(player.getCurrentLife() == INITIAL_LIFE + LIMIT)
    }

    @Test
    fun test04PlayerLoses1Life() {
        player.loss(1)
        assert(player.getCurrentLife() == INITIAL_LIFE - 1)
    }

    @Test
    fun test05PlayerLoseslifeMultipleTimes() {
        val LIMIT = 3
        for (i in 1..LIMIT) {
            player.loss(1)
        }
        assert(player.getCurrentLife() == INITIAL_LIFE - LIMIT)
    }

    @Test
    fun test06PlayerLosesTheGame() {
        player.loss(INITIAL_LIFE)
        assert(player.didLost())
    }

    @Test
    fun test07ResetGame() {
        player.gain(10)
        player.reset()
        assert(player.getDamageType() == DAMAGE_TYPE.LIFE)
        assert(player.getCurrentLife() == INITIAL_LIFE)
    }
}